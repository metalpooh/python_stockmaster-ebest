#-*- coding: utf-8 -*-
import sys
import win32com.client
import pythoncom
import csv
import sys

reload(sys)
sys.setdefaultencoding('utf-8')


class XASessionEvents:
    logInState = 0
    def OnLogin(self, code, msg):
        print("OnLogin method is called")
        print(str(code))
        print(str(msg))
        if str(code) == '0000':
            XASessionEvents.logInState = 1

    def OnLogout(self):
        print("OnLogout method is called")

    def OnDisconnect(self):
        print("OnDisconnect method is called")

class XAQueryEvents:
    queryState = 0
    def OnReceiveData(self, szTrCode):
        print("ReceiveData")
        XAQueryEvents.queryState = 1
    def OnReceiveMessage(self, systemError, mesageCode, message):
        print("ReceiveMessage")

if __name__ == "__main__":
    server_addr = "hts.etrade.co.kr"
    server_port = 20001
    server_type = 0
    user_id = ""
    user_pass = ""
    user_certificate_pass = ""

    #--------------------------------------------------------------------------
    # Login Session
    #--------------------------------------------------------------------------
    inXASession = win32com.client.DispatchWithEvents("XA_Session.XASession", XASessionEvents)
    inXASession.ConnectServer(server_addr, server_port)
    inXASession.Login(user_id, user_pass, user_certificate_pass, server_type, 0)

    while XASessionEvents.logInState == 0:
        pythoncom.PumpWaitingMessages()

    #--------------------------------------------------------------------------
    # Get single data 
    #--------------------------------------------------------------------------
    inXAQuery = win32com.client.DispatchWithEvents("XA_DataSet.XAQuery", XAQueryEvents)
    inXAQuery.LoadFromResFile("\\Res\\t8430.res")
    inXAQuery.SetFieldData('t8430InBlock', 'gubun', 0, 0)
    inXAQuery.Request(0)

    while XAQueryEvents.queryState == 0:
        pythoncom.PumpWaitingMessages()

    # Get FieldData
    nCount = inXAQuery.GetBlockCount('t8430OutBlock')
    with open("StockMaster0821_code.csv", "wb") as f:
        writer = csv.writer(f)
        count = 0
        count2 = 0
        for i in range(nCount):
            print(i, ":", inXAQuery.GetFieldData('t8430OutBlock', 'hname', i).encode("cp949"),':', inXAQuery.GetFieldData('t8430OutBlock', 'etfgubun', i))
            hname  = inXAQuery.GetFieldData('t8430OutBlock', 'hname', i)
            shcode  = 'A'+inXAQuery.GetFieldData('t8430OutBlock', 'shcode', i)
            etfgubun = inXAQuery.GetFieldData('t8430OutBlock', 'etfgubun', i)
            gubun = inXAQuery.GetFieldData('t8430OutBlock', 'gubun', i)
            expcode =inXAQuery.GetFieldData('t8430OutBlock', 'expcode', i)

            if etfgubun == u'1':
                continue
            elif etfgubun == u'2':
                continue

            if hname[-1] == u'호':
                continue
            if hname[-1] == u'팩':
                continue
            if hname[-2] == u'팩':
                continue
            if hname[-4:0] == u'SPAC':
                continue
            if hname[-1] == u'우':
                count2 = count2 + 1
            elif hname[-2] == u'우':
                count2 = count2 + 1

            count = count + 1
            writer.writerow([shcode,expcode,hname.encode("cp949"),etfgubun,gubun])


    XAQueryEvents.queryState = 0